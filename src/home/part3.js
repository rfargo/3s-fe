import React, {Component} from 'react';
import './index.css'

class HomePagePart3 extends Component{
 render(){
   return (
     <div className='container' id="part3">
     	<div>
     		<h2>Our Products</h2>

     		<div className="products" id="fire-safety">
				<h3>1. Fire Safety Products</h3>
     		</div>
     		<div className="products" id="eco-friendly">
				<h3>2. Eco-Friendly Products</h3>
     		</div>
     	</div>
    </div>
  )
 } 
}
export default HomePagePart3;