import React, {Component} from 'react';
import './index.css'

class HomePagePart1 extends Component{
 render(){
   return (
     <div className='container' id="part1">

       <div className='jumbotron'>
         <h1>Lorem Ipsum <br/> Dolor  Si Amet</h1>
          <p>
          consectetuer adipiscing elit, sed diam nonummy 
          nibh euismod tincidunt ut laoreet dolore magna 
          aliquam erat volutpat. Ut wisi enim ad minim veniam, 
          quis nostrud exerci tation ullamcorper suscipit 
          lobortis nisl ut aliquip ex ea commodo consequat.
		      </p>
    		 <a href="#" class="button" id="contact-us">Contact Us</a>
    		 <a href="#" class="button" id="see-product">See Our Products</a>
       </div>
       <div className="scroll-more"><p>V    Scroll for more    V</p></div>
    </div>
  )
 } 
}
export default HomePagePart1;