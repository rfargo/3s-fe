import React, {Component} from 'react';
import './index.css'
import HomePagePart1 from './part1';
import HomePagePart2 from './part2';
import HomePagePart3 from './part3';
import HomePagePart4 from './part4';

class HomePage extends Component{
 render(){
   return (
   	<div className="home">
	   	<HomePagePart1 />
	   	<HomePagePart2 />
	   	<HomePagePart3 />
	   	<HomePagePart4 />
   </div>
   )
 } 
}
export default HomePage;