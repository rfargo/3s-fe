import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Link, Switch
} from 'react-router-dom'
import HomePage from './home/index.js';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import Footer from './misc/footer.js';

function App() {
  return (
    <Router>
      <style>
      @import url('https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap');
      </style>
      <div className="App">
        <div className="header">
          <div>
            <div id="logo"></div>
            <div id="hamburger">
              <MenuIcon fontSize="large" style={{color:"white"}}></MenuIcon>
            </div>
          </div>



        </div>
        {/*<header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
        <Link to="/"> About </Link> */}

        <Switch>
          <Route exact path='/' component={HomePage}></Route>
        </Switch>

        <Footer/>

      </div>
    </Router>
  );
}

export default App;

/* https://medium.com/tech-tajawal/react-router-v4-in-minutes-b839d35f66f1 */

/*
          <div className="option-div">
            <p className="option">HOME</p>
            <p className="option">ABOUT US</p>
            <p className="option">PRODUCT</p>
            <p className="option">ACTIVITIES</p>
            <p className="option">CONTACT US</p>
          </div>

          <div>

          </div>
*/